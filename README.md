Sure! Here is a `README.md` file for using the Terraform module as a Git module over HTTPS.

### README.md

```markdown
# Proxmox VM Terraform Module

This Terraform module creates a Proxmox VM with a specified configuration, including default SSH keys, CPU, memory, disk size, and network settings.

## Features

- Create a Proxmox VM from a template.
- Configure SSH keys, CPU, memory, and disk size.
- Set up network bridge and VLAN tag.
- Use cloud-init for initial setup.

## Usage

### Example Configuration

Create a new Terraform configuration directory and include the module:

```hcl
module "metrics_db_vm" {
  source          = "git::https://gitlab.com/spakl/registry/proxmox_vm?ref=master"
  vmid            = 900
  name            = "metrics_db"
  tags            = "metrics;db"
  target_node     = "proxmox-2"
  desc            = "metrics_db"
  vm_user         = "root"
  vm_passwd       = "your-password"
  clone_name      = "ubuntu-cloud"
  cores           = 1
  sockets         = 1
  memory          = 1024
  disk_size       = "80G"
  disk_storage    = "local-lvm"
  network_bridge  = "vmbr0"
  vlan_tag        = 10
  ip_address      = "192.168.10.100"
  subnet          = "24"
}
```

### Variables

| Variable       | Description                           | Type   | Default        |
| -------------- | ------------------------------------- | ------ | -------------- |
| `vmid`         | The unique ID of the VM.              | number | `900`          |
| `name`         | The name of the VM.                   | string | `"metrics_db"` |
| `tags`         | Tags for the VM.                      | string | `"metrics;db"` |
| `target_node`  | The Proxmox node to deploy the VM to. | string | `"proxmox-2"`  |
| `desc`         | Description of the VM.                | string | `"metrics_db"` |
| `vm_user`      | The default user for the VM.          | string | `"root"`       |
| `vm_passwd`    | The password for the default user.    | string | n/a            |
| `clone_name`   | The name of the VM/template to clone. | string | `"ubuntu-cloud"`|
| `cores`        | The number of CPU cores for the VM.   | number | `1`            |
| `sockets`      | The number of CPU sockets for the VM. | number | `1`            |
| `memory`       | The amount of memory for the VM in MB.| number | `1024`         |
| `disk_size`    | The size of the VM disk in GB.        | string | `"80G"`        |
| `disk_storage` | The storage location for the VM disk. | string | n/a            |
| `network_bridge`| The network bridge for the VM.       | string | n/a            |
| `vlan_tag`     | The VLAN tag for the VM.              | number | n/a            |
| `ip_address`   | The IP address for the VM.            | string | n/a            |
| `subnet`       | The subnet for the VM IP address.     | string | n/a            |

### Outputs

| Output        | Description                            |
| ------------- | -------------------------------------- |
| `vmid`        | The VM ID.                             |
| `name`        | The name of the VM.                    |
| `ip_address`  | The IP address of the VM.              |
| `memory`      | The amount of memory allocated to the VM in MB. |
| `cores`       | The number of CPU cores allocated to the VM.    |
| `sockets`     | The number of CPU sockets allocated to the VM.  |
| `cpu_type`    | The type of CPU for the VM.            |
| `disk_size`   | The size of the VM disk in GB.         |
| `disk_storage`| The storage location of the VM disk.   |
| `network_bridge`| The network bridge used by the VM.   |
| `vlan_tag`    | The VLAN tag for the VM.               |
| `nameserver`  | The nameserver used by the VM.         |
| `agent`       | The QEMU guest agent status.           |
| `os_type`     | The operating system type of the VM.   |
| `status`      | The current status of the VM.          |
| `tags`        | The tags associated with the VM.       |

