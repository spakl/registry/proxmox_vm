variable "vmid" {
  description = "The unique ID of the VM."
  type        = number
}

variable "name" {
  description = "The name of the VM."
  type        = string
}

variable "tags" {
  description = "Tags for the VM."
  type        = string
  default     = ""
}

variable "target_node" {
  description = "The Proxmox node to deploy the VM to."
  type        = string
  default     = "proxmox"
}

variable "desc" {
  description = "Description of the VM."
  type        = string
  default     = "Created using module https://gitlab.com/spakl/registry/proxmox_vm"
}

variable "vm_user" {
  description = "The default user for the VM."
  type        = string
  default     = "neo"
}

variable "vm_passwd" {
  description = "The password for the default user."
  type        = string
}

variable "clone_name" {
  description = "The name of the VM/template to clone."
  type        = string
  default     = "ubuntu-cloud"
}

variable "cores" {
  description = "The number of CPU cores for the VM."
  type        = number
  default     = 1
}

variable "sockets" {
  description = "The number of CPU sockets for the VM."
  type        = number
  default     = 1
}

variable "memory" {
  description = "The amount of memory for the VM in MB."
  type        = number
  default     = 1024
}

variable "disk_size" {
  description = "The size of the VM disk in GB."
  type        = string
  default     = "80G"
}

variable "disk_storage" {
  description = "The storage location for the VM disk."
  type        = string
}

variable "network_bridge" {
  description = "The network bridge for the VM."
  type        = string
  default     = "vmbr0"
}

variable "vlan_tag" {
  description = "The VLAN tag for the VM."
  type        = number
}

variable "ip_address" {
  description = "The IP address for the VM."
  type        = string
}

variable "subnet" {
  description = "The subnet for the VM IP address."
  type        = string
  default     = "24"
}
