output "vmid" {
  description = "The VM ID."
  value       = proxmox_vm_qemu.vm.vmid
}

output "name" {
  description = "The name of the VM."
  value       = proxmox_vm_qemu.vm.name
}

output "ip_address" {
  description = "The IP address of the VM."
  value       = var.ip_address
}

output "memory" {
  description = "The amount of memory allocated to the VM in MB."
  value       = proxmox_vm_qemu.vm.memory
}

output "cores" {
  description = "The number of CPU cores allocated to the VM."
  value       = proxmox_vm_qemu.vm.cores
}

output "sockets" {
  description = "The number of CPU sockets allocated to the VM."
  value       = proxmox_vm_qemu.vm.sockets
}

output "cpu_type" {
  description = "The type of CPU for the VM."
  value       = proxmox_vm_qemu.vm.cpu
}

output "network_bridge" {
  description = "The network bridge used by the VM."
  value       = proxmox_vm_qemu.vm.network[0].bridge
}

output "vlan_tag" {
  description = "The VLAN tag for the VM."
  value       = proxmox_vm_qemu.vm.network[0].tag
}

output "nameserver" {
  description = "The nameserver used by the VM."
  value       = proxmox_vm_qemu.vm.nameserver
}

output "agent" {
  description = "The QEMU guest agent status."
  value       = proxmox_vm_qemu.vm.agent
}

output "os_type" {
  description = "The operating system type of the VM."
  value       = proxmox_vm_qemu.vm.os_type
}

output "tags" {
  description = "The tags associated with the VM."
  value       = proxmox_vm_qemu.vm.tags
}
