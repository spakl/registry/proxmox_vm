terraform {
  required_providers {
    proxmox = {
      source = "Telmate/proxmox"
      version = "3.0.1-rc3"
    }
  }
}


resource "proxmox_vm_qemu" "vm" {
  vmid        = var.vmid
  name        = var.name
  tags        = var.tags
  target_node = var.target_node
  desc        = var.desc
  os_type     = "cloud-init"
  ciuser      = var.vm_user
  cipassword  = var.vm_passwd
  sshkeys     = file("${path.module}/sshkeys")
  clone       = var.clone_name
  onboot      = true
  agent       = 0
  cores       = var.cores
  sockets     = var.sockets
  cpu         = "host"
  memory      = var.memory
  disks {
    virtio {
        virtio0{
            disk{
                backup  = true
                cache   = "none"
                format  = "raw"
                size    = var.disk_size
                storage = var.disk_storage
            }
        }
    }
  }
  network {
    bridge = var.network_bridge
    model  = "virtio"
    tag    = var.vlan_tag
  }
  ipconfig0  = "ip=${var.ip_address}/${var.subnet},gw=10.10.${tostring(var.vlan_tag)}.1"
  nameserver = "10.10.${tostring(var.vlan_tag)}.1"
}
